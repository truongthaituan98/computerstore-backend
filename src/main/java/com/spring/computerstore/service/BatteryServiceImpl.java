package com.spring.computerstore.service;

import com.spring.computerstore.exception.CustomException;
import com.spring.computerstore.model.Battery;
import com.spring.computerstore.repository.BatteryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BatteryServiceImpl implements BatteryService {
    @Autowired
    BatteryRepository batteryRepository;
    @Override
    public List<Battery> getAllBattery() {
        return batteryRepository.findAll();
    }

    @Override
    public Battery findBatteryById(Long battery_id) {
        Optional<Battery> batteryOptional = batteryRepository.findById(battery_id);
        if (batteryOptional.isPresent()){
            return batteryOptional.get();
        }else{
            throw new CustomException(HttpStatus.NOT_FOUND.value(),"Battery with id "+battery_id+" not found!");
        }
    }

    @Override
    public void deleteBatteryById(Long battery_id) {
        batteryRepository.delete(findBatteryById(battery_id));
    }

    @Override
    public Battery saveOrUpdate(Battery battery) {
        return batteryRepository.save(battery);
    }
}
