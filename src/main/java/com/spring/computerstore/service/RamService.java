package com.spring.computerstore.service;

import com.spring.computerstore.model.Ram;

import java.util.List;

public interface RamService {
    List<Ram> getAllRam();

    Ram findRamById(Long ramId);

    void deleteRamById(Long ramId);

    Ram saveOrUpdate(Ram ram);
}
