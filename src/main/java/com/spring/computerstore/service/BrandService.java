package com.spring.computerstore.service;

import com.spring.computerstore.model.Brand;

import java.util.List;

public interface BrandService {
    List<Brand> getAllBrand();

    Brand findBrandById(Long brand_id);

    void deleteBrandById(Long brand_id);

    Brand saveOrUpdate(Brand brand);
}
