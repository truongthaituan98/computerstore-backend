package com.spring.computerstore.service;

import com.spring.computerstore.dto.AccountUpdateDTO;
import com.spring.computerstore.dto.AccountDTO;
import com.spring.computerstore.dto.ResponseAuth;
import com.spring.computerstore.model.Account;

import java.util.List;

public interface AccountService {
    List<AccountDTO> findAll();
    AccountDTO getUserById(Long id);
    AccountDTO registerUser(Account account);
    AccountDTO updateUser(Account account);
    void deleteUser(Long id);
    AccountDTO findByuserName(String userName);
    AccountDTO changePassword(AccountUpdateDTO accountUpdateDTO);
    int emailAlreadyExist(String email);
    int isUserNameExist(String userName);
    ResponseAuth login(String userName, String password);
}
