package com.spring.computerstore.service;

import com.spring.computerstore.model.Screen;

import java.util.List;

public interface ScreenService {
    List<Screen> getAllScreen();

    Screen findScreenById(Long screenId);

    void deleteScreenById(Long screenId);

    Screen saveOrUpdate(Screen screen);
}
