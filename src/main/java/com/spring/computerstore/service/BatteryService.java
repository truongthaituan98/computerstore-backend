package com.spring.computerstore.service;

import com.spring.computerstore.model.Battery;

import java.util.List;

public interface BatteryService {
    List<Battery> getAllBattery();

    Battery findBatteryById(Long battery_id);

    void deleteBatteryById(Long battery_id);

    Battery saveOrUpdate(Battery battery);
}
