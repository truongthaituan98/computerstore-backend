package com.spring.computerstore.service;

import com.spring.computerstore.model.GraphicsCard;

import java.util.List;

public interface GraphicsCardService {
    List<GraphicsCard> getAllGraphicsCard();

    GraphicsCard findGraphicsCardById(Long graphicsCardId);

    void deleteGraphicsCardById(Long graphicsCardId);

    GraphicsCard saveOrUpdate(GraphicsCard graphicsCard);
}
