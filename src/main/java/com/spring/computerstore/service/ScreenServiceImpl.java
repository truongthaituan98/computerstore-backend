package com.spring.computerstore.service;

import com.spring.computerstore.exception.CustomException;
import com.spring.computerstore.model.Screen;
import com.spring.computerstore.repository.ScreenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ScreenServiceImpl implements ScreenService {

    @Autowired
    ScreenRepository screenRepository;

    @Override
    public List<Screen> getAllScreen() {
        return screenRepository.findAll();
    }

    @Override
    public Screen findScreenById(Long screenId) {
        Optional<Screen> screenOptional = screenRepository.findById(screenId);
        if (screenOptional.isPresent()){
            return screenOptional.get();
        } else {
            throw new CustomException(HttpStatus.NOT_FOUND.value(),"Screen with id "+screenId+" not found!");
        }
    }

    @Override
    public void deleteScreenById(Long screenId) {
        screenRepository.delete(findScreenById(screenId));
    }

    @Override
    public Screen saveOrUpdate(Screen screen) {
        return screenRepository.save(screen);
    }
}
