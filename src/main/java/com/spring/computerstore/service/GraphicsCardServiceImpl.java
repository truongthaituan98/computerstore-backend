package com.spring.computerstore.service;

import com.spring.computerstore.exception.CustomException;
import com.spring.computerstore.model.GraphicsCard;
import com.spring.computerstore.repository.BatteryRepository;
import com.spring.computerstore.repository.GraphicsCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GraphicsCardServiceImpl implements GraphicsCardService{
    @Autowired
    GraphicsCardRepository graphicsCardRepository;
    @Override
    public List<GraphicsCard> getAllGraphicsCard() {
        return graphicsCardRepository.findAll();
    }

    @Override
    public GraphicsCard findGraphicsCardById(Long graphicsCardId) {
        Optional<GraphicsCard> graphicsCardOptional = graphicsCardRepository.findById(graphicsCardId);
        if (graphicsCardOptional.isPresent()){
            return graphicsCardOptional.get();
        } else {
            throw new CustomException(HttpStatus.NOT_FOUND.value(),"Graphics Card with id "+graphicsCardId+" not found!");
        }
    }

    @Override
    public void deleteGraphicsCardById(Long graphicsCardId) {
        graphicsCardRepository.delete(findGraphicsCardById(graphicsCardId));
    }

    @Override
    public GraphicsCard saveOrUpdate(GraphicsCard graphicsCard) {
        return graphicsCardRepository.save(graphicsCard);
    }
}
