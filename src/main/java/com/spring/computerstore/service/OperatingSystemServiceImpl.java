package com.spring.computerstore.service;

import com.spring.computerstore.exception.CustomException;
import com.spring.computerstore.model.OperatingSystem;
import com.spring.computerstore.repository.OperatingSystemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OperatingSystemServiceImpl implements OperatingSystemService {
    @Autowired
    OperatingSystemRepository operatingSystemRepository;
    @Override
    public List<OperatingSystem> getAllOperatingSystem() {
        return operatingSystemRepository.findAll();
    }

    @Override
    public OperatingSystem findOperatingSystemById(Long operatingSystemId) {
        Optional<OperatingSystem> operatingSystemOptional = operatingSystemRepository.findById(operatingSystemId);
        if (operatingSystemOptional.isPresent()){
            return operatingSystemOptional.get();
        } else {
            throw new CustomException(HttpStatus.NOT_FOUND.value(),"Operating System with id "+operatingSystemId+" not found!");
        }
    }

    @Override
    public void deleteOperatingSystemById(Long operatingSystemId) {
        operatingSystemRepository.delete(findOperatingSystemById(operatingSystemId));
    }

    @Override
    public OperatingSystem saveOrUpdate(OperatingSystem operatingSystem) {
        return operatingSystemRepository.save(operatingSystem);
    }
}
