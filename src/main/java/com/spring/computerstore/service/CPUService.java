package com.spring.computerstore.service;

import com.spring.computerstore.model.CPU;

import java.util.List;

public interface CPUService {
    List<CPU> getCPUList();

    CPU findCPUById(Long cpu_id);

    void deleteCPUById(Long cpu_id);

    CPU saveOrUpdate(CPU cpu);
}
