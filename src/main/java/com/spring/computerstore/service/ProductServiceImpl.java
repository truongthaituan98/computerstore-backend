package com.spring.computerstore.service;

import com.spring.computerstore.exception.CustomException;
import com.spring.computerstore.model.Product;
import com.spring.computerstore.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;
    @Override
    public List<Product> getAllProduct() {
        return productRepository.findAll();
    }

    @Override
    public Product findProductById(Long productId) {
        Optional<Product> productOptional = productRepository.findById(productId);
        if (productOptional.isPresent()){
            return productOptional.get();
        } else {
            throw new CustomException(HttpStatus.NOT_FOUND.value(),"Product with id "+productId+" not found!");
        }
    }

    @Override
    public void deleteProductById(Long productId) {
        productRepository.delete(findProductById(productId));
    }

    @Override
    public Product saveOrUpdate(Product product) {
        return productRepository.save(product);
    }
}
