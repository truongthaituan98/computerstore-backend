package com.spring.computerstore.service;

import com.spring.computerstore.exception.CustomException;
import com.spring.computerstore.model.Brand;
import com.spring.computerstore.repository.BrandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BrandServiceImpl implements BrandService {
    @Autowired
    BrandRepository brandRepository;
    @Override
    public List<Brand> getAllBrand() {
        return brandRepository.findAll();
    }

    @Override
    public Brand findBrandById(Long brand_id) {
        Optional<Brand> brandOptional = brandRepository.findById(brand_id);
        if (brandOptional.isPresent()){
            return brandOptional.get();
        }else{
            throw new CustomException(HttpStatus.NOT_FOUND.value(),"Brand with id "+brand_id+" not found!");
        }
    }

    @Override
    public void deleteBrandById(Long brand_id) {
        brandRepository.delete(findBrandById(brand_id));
    }

    @Override
    public Brand saveOrUpdate(Brand brand) {
        return brandRepository.save(brand);
    }
}
