package com.spring.computerstore.service;

import com.spring.computerstore.model.OperatingSystem;

import java.util.List;

public interface OperatingSystemService {
    List<OperatingSystem> getAllOperatingSystem();

    OperatingSystem findOperatingSystemById(Long operatingSystemId);

    void deleteOperatingSystemById(Long operatingSystemId);

    OperatingSystem saveOrUpdate(OperatingSystem operatingSystem);
}
