package com.spring.computerstore.service;

import com.spring.computerstore.exception.CustomException;
import com.spring.computerstore.model.Ram;
import com.spring.computerstore.repository.RamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RamServiceImpl implements RamService{
    @Autowired
    RamRepository ramRepository;
    @Override
    public List<Ram> getAllRam() {
        return ramRepository.findAll();
    }

    @Override
    public Ram findRamById(Long ramId) {
        Optional<Ram> ramOptional = ramRepository.findById(ramId);
        if (ramOptional.isPresent()){
            return ramOptional.get();
        } else {
            throw new CustomException(HttpStatus.NOT_FOUND.value(),"Ram with id "+ramId+" not found!");
        }
    }

    @Override
    public void deleteRamById(Long ramId) {
        ramRepository.delete(findRamById(ramId));
    }

    @Override
    public Ram saveOrUpdate(Ram ram) {
        return ramRepository.save(ram);
    }
}
