package com.spring.computerstore.service;

import com.spring.computerstore.model.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAllProduct();

    Product findProductById(Long productId);

    void deleteProductById(Long productId);

    Product saveOrUpdate(Product product);
}
