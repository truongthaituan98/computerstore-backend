package com.spring.computerstore.service;

import com.spring.computerstore.model.HardDisk;

import java.util.List;

public interface HardDiskService {
    List<HardDisk> getAllHardDisk();

    HardDisk findHardDiskById(Long hardDiskId);

    void deleteHardDiskById(Long hardDiskId);

    HardDisk saveOrUpdate(HardDisk hardDisk);
}
