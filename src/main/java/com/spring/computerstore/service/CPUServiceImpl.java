package com.spring.computerstore.service;

import com.spring.computerstore.exception.CustomException;
import com.spring.computerstore.model.CPU;
import com.spring.computerstore.repository.CPURepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CPUServiceImpl implements CPUService {

    @Autowired
    CPURepository cpuRepository;

    @Override
    public List<CPU> getCPUList() {
        return cpuRepository.findAll();
    }

    @Override
    public CPU findCPUById(Long cpu_id) {
        Optional<CPU> cpuOptional = cpuRepository.findById(cpu_id);
        if (cpuOptional.isPresent()){
            return cpuOptional.get();
        }else{
            throw new CustomException(HttpStatus.NOT_FOUND.value(),"CPU with id "+cpu_id+" not found!");
        }
    }

    @Override
    public void deleteCPUById(Long cpu_id) {
        cpuRepository.delete(findCPUById(cpu_id));
    }

    @Override
    public CPU saveOrUpdate(CPU cpu) {
        return cpuRepository.save(cpu);
    }
}
