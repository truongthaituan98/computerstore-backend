package com.spring.computerstore.service;

import com.spring.computerstore.exception.CustomException;
import com.spring.computerstore.model.HardDisk;
import com.spring.computerstore.repository.HardDiskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HardDiskServiceImpl implements HardDiskService{
    @Autowired
    HardDiskRepository hardDiskRepository;
    @Override
    public List<HardDisk> getAllHardDisk() {
        return hardDiskRepository.findAll();
    }

    @Override
    public HardDisk findHardDiskById(Long hardDiskId) {
        Optional<HardDisk> hardDiskOptional = hardDiskRepository.findById(hardDiskId);
        if (hardDiskOptional.isPresent()){
            return hardDiskOptional.get();
        } else {
            throw new CustomException(HttpStatus.NOT_FOUND.value(),"Hard Disk with id "+hardDiskId+" not found!");
        }
    }

    @Override
    public void deleteHardDiskById(Long hardDiskId) {
        hardDiskRepository.delete(findHardDiskById(hardDiskId));
    }

    @Override
    public HardDisk saveOrUpdate(HardDisk hardDisk) {
        return hardDiskRepository.save(hardDisk);
    }
}
