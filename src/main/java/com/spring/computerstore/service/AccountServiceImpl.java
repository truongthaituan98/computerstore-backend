package com.spring.computerstore.service;

import com.spring.computerstore.common.MapperUtil;
import com.spring.computerstore.dto.AccountDTO;
import com.spring.computerstore.dto.AccountUpdateDTO;
import com.spring.computerstore.dto.ResponseAuth;
import com.spring.computerstore.exception.CustomException;
import com.spring.computerstore.model.Account;
import com.spring.computerstore.model.Role;
import com.spring.computerstore.repository.AccountRepository;
import com.spring.computerstore.repository.RoleRepository;
import com.spring.computerstore.security.CustomUserDetails;
import com.spring.computerstore.security.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    AccountRepository accountRepository;

    @Override
    public List<AccountDTO> findAll() {
        return MapperUtil.mapListAccountToAccountDTO(accountRepository.findAll());
    }

    @Override
    public AccountDTO getUserById(Long id) {
        Optional<Account> batteryOptional = accountRepository.findById(id);
        if (batteryOptional.isPresent()){
            return MapperUtil.accountToAccountDTO(batteryOptional.get());
        }else{
            throw new CustomException(HttpStatus.NOT_FOUND.value(),"Battery with id "+id+" not found!");
        }
    }

    @Override
    public AccountDTO registerUser(Account account) {
        if(emailAlreadyExist(account.getEmail()) == 1) {
            throw new CustomException(HttpStatus.BAD_REQUEST.value(), "Email is already exist");
        }
        if(isUserNameExist(account.getUserName()) == 1) {
            throw new CustomException(HttpStatus.BAD_REQUEST.value(), "Username is already exist");
        }
        account.setPassword(new BCryptPasswordEncoder().encode(account.getPassword()));
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepository.getById(1L));
        account.setUserName(account.getUserName());
        account.setEmail(account.getEmail());
        account.setFullName(account.getFullName());
        account.setGender(account.getGender());
        account.setStatus(true);
        account.setRoles(roles);
        return MapperUtil.accountToAccountDTO(accountRepository.save(account));
    }

    @Override
    public AccountDTO updateUser(Account account) {
       Account curentAccount = accountRepository.getById(account.getAccountId());
       Set<Role> roles = new HashSet<>();
       for (Role role : account.getRoles()){
           roles.add(roleRepository.getById(role.getRoleId()));
       }
       curentAccount.setRoles(roles);
       curentAccount.setEmail(account.getEmail());
       curentAccount.setFullName(account.getFullName());
       curentAccount.setUserName(account.getUserName());
       curentAccount.setGender(account.getGender());
       try {
          curentAccount = accountRepository.save(curentAccount);
       }catch (Exception ex){

       }
       return MapperUtil.accountToAccountDTO(curentAccount);
    }

    @Override
    public void deleteUser(Long id) {
        AccountDTO accountDTO = getUserById(id);
        if(accountDTO != null) {
            accountRepository.deleteAccountsByAccountId(id, false);
        }
    }

    @Override
    public AccountDTO findByuserName(String userName) {
        Account account = accountRepository.findByuserName(userName);
        if(account == null){
           return null;
        }
        return MapperUtil.accountToAccountDTO(account);
    }

    @Override
    public AccountDTO changePassword(AccountUpdateDTO accountUpdateDTO) {
        return null;
    }

    @Override
    public int emailAlreadyExist(String email) {
        return accountRepository.isEmailExist(email);
    }

    @Override
    public int isUserNameExist(String userName) {
        return accountRepository.isUserNameExist(userName);
    }

    @Override
    public ResponseAuth login(String userName, String password) {
        ResponseAuth response;
        try {
            Authentication authentication =
                    authenticationManager
                            .authenticate(
                                    new UsernamePasswordAuthenticationToken(userName, password));
            // Inject current user into security context
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String roles =  String.valueOf(SecurityContextHolder.getContext().getAuthentication().getAuthorities());
            roles.trim();
            roles = roles.substring(1);
            roles = roles.substring(0, roles.length() - 1);
            response = new ResponseAuth(jwtUtil.generateTokenForUser((CustomUserDetails) authentication.getPrincipal()),true,
                    userName,roles);
        } catch (AuthenticationException ex) {
            response = new ResponseAuth("",false,
                    "","");
//            throw new CustomException(HttpStatus.BAD_REQUEST.value(),"Loi");
        }
        return response;
    }
}
