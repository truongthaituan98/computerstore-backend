package com.spring.computerstore.security;

import com.spring.computerstore.exception.CustomException;
import com.spring.computerstore.model.Account;
import com.spring.computerstore.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailService implements UserDetailsService {
    @Autowired
    AccountRepository accountRepository;
    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Account account = accountRepository.findByuserName(userName);
        if(account == null) {
            throw new CustomException(HttpStatus.NOT_FOUND.value(), "Username not found");
        }
        return new CustomUserDetails(account);
    }
}
