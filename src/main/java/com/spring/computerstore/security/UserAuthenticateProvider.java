package com.spring.computerstore.security;

import com.spring.computerstore.exception.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class UserAuthenticateProvider implements AuthenticationProvider {

    @Autowired
    CustomUserDetailService customUserDetailService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            UserDetails userDetails = customUserDetailService.loadUserByUsername(authentication.getName());
            //Check user
            UsernamePasswordAuthenticationToken result = null;
            if(userDetails != null){
                if(userDetails.getUsername().equals(authentication.getName()) && userDetails.getPassword().equals(authentication.getCredentials().toString())) {
                    result = new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());
                }
            }
            return result;
        } catch (Exception ex) {
            throw new CustomException(HttpStatus.UNAUTHORIZED.value(), "User authentication failed");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
