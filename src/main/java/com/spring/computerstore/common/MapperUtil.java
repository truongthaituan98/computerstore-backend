package com.spring.computerstore.common;

import com.spring.computerstore.dto.AccountDTO;
import com.spring.computerstore.model.Account;

import java.util.ArrayList;
import java.util.List;

public class MapperUtil {
    public static AccountDTO accountToAccountDTO(Account account) {
        AccountDTO accountDTO  = new AccountDTO(account.getUserName(), account.getFullName(), account.getEmail(), account.getGender(), account.getStatus());
        return accountDTO;
    }
    public static List<AccountDTO> mapListAccountToAccountDTO(List<Account> accountList) {
        List<AccountDTO> accountDTOList = new ArrayList<>();
        for (Account account : accountList){
            AccountDTO accountDTO  = new AccountDTO(account.getUserName(), account.getFullName(), account.getEmail(), account.getGender(), account.getStatus());
            accountDTOList.add(accountDTO);
        }
        return accountDTOList;
    }
}
