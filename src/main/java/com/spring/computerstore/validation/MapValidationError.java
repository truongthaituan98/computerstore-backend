package com.spring.computerstore.validation;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import java.util.HashMap;
import java.util.Map;

@Service
public class MapValidationError {
    public ResponseEntity<?> mapValidationError(BindingResult bindingResult) {
        if(bindingResult.hasErrors()){
            Map<String, String> mapError = new HashMap<>();
            for(FieldError fieldError: bindingResult.getFieldErrors()){
                mapError.put(fieldError.getField(), fieldError.getDefaultMessage());
            }
            return new ResponseEntity<>(mapError, HttpStatus.BAD_REQUEST);
        }
       return null;
    }
}
