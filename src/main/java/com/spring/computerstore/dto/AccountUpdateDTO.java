package com.spring.computerstore.dto;

public class AccountUpdateDTO {
    private AccountDTO accountDTO;
    private String currentPassword;
    private String newPassword;

    public AccountUpdateDTO(AccountDTO accountDTO, String currentPassword, String newPassword) {
        this.accountDTO = accountDTO;
        this.currentPassword = currentPassword;
        this.newPassword = newPassword;
    }

    public AccountUpdateDTO() {
    }

    public AccountDTO getAccountDTO() {
        return accountDTO;
    }

    public void setAccountDTO(AccountDTO accountDTO) {
        this.accountDTO = accountDTO;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
