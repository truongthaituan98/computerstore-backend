package com.spring.computerstore.dto;

public class ResponseAuth {
    private String token;
    private Boolean isLoggedIn;
    private String userName;
    private String roles;

    public ResponseAuth(String token, Boolean isLoggedIn, String userName, String roles) {
        this.token = token;
        this.isLoggedIn = isLoggedIn;
        this.userName = userName;
        this.roles = roles;
    }

    public ResponseAuth() { }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(Boolean loggedIn) {
        isLoggedIn = loggedIn;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }
}
