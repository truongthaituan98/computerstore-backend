package com.spring.computerstore.dto;

public class AccountDTO {
    private String userName;
    private String fullName;
    private String email;
    private Boolean gender;
    private Boolean status;
    public AccountDTO(String userName, String fullName, String email, Boolean gender, Boolean status) {
        this.userName = userName;
        this.fullName = fullName;
        this.email = email;
        this.gender = gender;
        this.status = status;
    }

    public AccountDTO() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
