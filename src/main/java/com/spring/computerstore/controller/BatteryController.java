package com.spring.computerstore.controller;

import com.spring.computerstore.model.Battery;
import com.spring.computerstore.service.BatteryService;
import com.spring.computerstore.validation.MapValidationError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/api/battery")
@RestController
public class BatteryController {
    @Autowired
    BatteryService batteryService;

    @Autowired
    MapValidationError mapValidationError;
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<Battery>> getAllBattery() {
        List<Battery> batteryList = batteryService.getAllBattery();
        return new ResponseEntity<>(batteryList, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/save", method = RequestMethod.POST)
    public ResponseEntity<?> saveBattery(@Valid @RequestBody Battery battery, BindingResult bindingResult) {
        ResponseEntity<?> errorMap = mapValidationError.mapValidationError(bindingResult);
        if(errorMap!=null) return errorMap;
        Battery batterySave = batteryService.saveOrUpdate(battery);
        return new ResponseEntity<>(batterySave, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Battery> getBatteryById(@PathVariable("id") Long batteryId) {
        Battery battery = batteryService.findBatteryById(batteryId);
        return new ResponseEntity<>(battery, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Battery> updateBattery(@PathVariable("id") Long batteryId, @RequestBody Battery battery) {
        Battery batteryCurrent = batteryService.findBatteryById(batteryId);
        batteryCurrent.setBatteryInfo(battery.getBatteryInfo());
        batteryCurrent.setBatteryExpiryDate(battery.getBatteryExpiryDate());
        batteryCurrent.setCharger(battery.getCharger());
        Battery batteryUpdate = batteryService.saveOrUpdate(batteryCurrent);
        return new ResponseEntity<>(batteryUpdate, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteBatteryById(@PathVariable("id") Long batteryId) {
        batteryService.deleteBatteryById(batteryId);
        return new ResponseEntity<>("Battery with ID "+batteryId+" was deleted", HttpStatus.OK);
    }
}
