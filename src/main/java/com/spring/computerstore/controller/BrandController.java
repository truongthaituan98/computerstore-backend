package com.spring.computerstore.controller;

import com.spring.computerstore.model.Brand;
import com.spring.computerstore.service.BrandService;
import com.spring.computerstore.validation.MapValidationError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/api/brand")
public class BrandController {
    @Autowired
    BrandService brandService;
    @Autowired
    MapValidationError mapValidationError;
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<Brand>> getAllBrand() {
        List<Brand> brandList = brandService.getAllBrand();
        return new ResponseEntity<>(brandList, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/save", method = RequestMethod.POST)
    public ResponseEntity<?> saveBrand(@Valid @RequestBody Brand brand, BindingResult bindingResult) {
        ResponseEntity<?> errorMap = mapValidationError.mapValidationError(bindingResult);
        if(errorMap!=null) return errorMap;
        Brand brand1 = brandService.saveOrUpdate(brand);
        return new ResponseEntity<>(brand1, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Brand> getBrandById(@PathVariable("id") Long brandId) {
        Brand brand = brandService.findBrandById(brandId);
        return new ResponseEntity<>(brand, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Brand> updateBrand(@PathVariable("id") Long brandId, @RequestBody Brand brand) {
        Brand brandCurrent = brandService.findBrandById(brandId);
        brandCurrent.setName(brand.getName());
        Brand brandUpdate = brandService.saveOrUpdate(brandCurrent);
        return new ResponseEntity<>(brandUpdate, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteBrandById(@PathVariable("id") Long brandId) {
        brandService.deleteBrandById(brandId);
        return new ResponseEntity<>("Brand with ID "+brandId+" was deleted", HttpStatus.OK);
    }
}
