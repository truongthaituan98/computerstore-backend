package com.spring.computerstore.controller;

import com.spring.computerstore.model.GraphicsCard;
import com.spring.computerstore.service.GraphicsCardService;
import com.spring.computerstore.validation.MapValidationError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/api/graphics_card")
@RestController
public class GraphicsCardController {
    @Autowired
    GraphicsCardService graphicsCardService;
    @Autowired
    MapValidationError mapValidationError;
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<GraphicsCard>> getAllGraphicsCard() {
        List<GraphicsCard> graphicsCardList = graphicsCardService.getAllGraphicsCard();
        return new ResponseEntity<>(graphicsCardList, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/save", method = RequestMethod.POST)
    public ResponseEntity<?> saveGraphicsCard(@Valid @RequestBody GraphicsCard graphicsCard, BindingResult bindingResult) {
        ResponseEntity<?> errorMap = mapValidationError.mapValidationError(bindingResult);
        if(errorMap!=null) return errorMap;
        GraphicsCard graphicsCard1 = graphicsCardService.saveOrUpdate(graphicsCard);
        return new ResponseEntity<>(graphicsCard1, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<GraphicsCard> getGraphicsCardById(@PathVariable("id") Long graphicsCardId) {
        GraphicsCard graphicsCard = graphicsCardService.findGraphicsCardById(graphicsCardId);
        return new ResponseEntity<>(graphicsCard, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.PUT)
    public ResponseEntity<GraphicsCard> updateGraphicsCard(@PathVariable("id") Long graphicsCardId, @RequestBody GraphicsCard graphicsCard) {
        GraphicsCard graphicsCardCurrent = graphicsCardService.findGraphicsCardById(graphicsCardId);
        graphicsCardCurrent.setName(graphicsCard.getName());
        graphicsCardCurrent.setMemory(graphicsCard.getMemory());
        graphicsCardCurrent.setType(graphicsCard.getType());
        GraphicsCard gaphicsCardUpdate = graphicsCardService.saveOrUpdate(graphicsCardCurrent);
        return new ResponseEntity<>(gaphicsCardUpdate, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteGraphicsCardById(@PathVariable("id") Long graphicsCardId) {
        graphicsCardService.deleteGraphicsCardById(graphicsCardId);
        return new ResponseEntity<>("Graphics Card with ID "+graphicsCardId+" was deleted", HttpStatus.OK);
    }
}
