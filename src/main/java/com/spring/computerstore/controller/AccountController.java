package com.spring.computerstore.controller;

import com.spring.computerstore.dto.AccountDTO;
import com.spring.computerstore.exception.CustomException;
import com.spring.computerstore.model.Account;
import com.spring.computerstore.service.AccountService;
import com.spring.computerstore.validation.MapValidationError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/api/account")
public class AccountController {
    @Autowired
    AccountService accountService;
    @Autowired
    private MapValidationError mapValidationError;
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<AccountDTO>> findUsers() {
        List<AccountDTO> userDTOS = accountService.findAll();
        if (userDTOS.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(userDTOS, HttpStatus.OK);
    }

    @RequestMapping(value = "/{userName}", method = RequestMethod.GET)
    public ResponseEntity<?> findUserByUsername(@PathVariable("userName") String userName) {
        return new ResponseEntity<>(accountService.findByuserName(userName), HttpStatus.OK);
    }

    @RequestMapping(value = "/register",
            method = RequestMethod.POST)
    public ResponseEntity<?> createUser(@RequestBody @Valid Account account, BindingResult bindingResult) {
        ResponseEntity<?> errorMap = mapValidationError.mapValidationError(bindingResult);
        if(errorMap != null) return errorMap;
        AccountDTO accountDTO = accountService.registerUser(account);
        return new ResponseEntity<>(accountDTO, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteAccount(@PathVariable("id") Long accountId) {
        accountService.deleteUser(accountId);
        return new ResponseEntity<>("Delete account successfully", HttpStatus.OK);
    }
}
