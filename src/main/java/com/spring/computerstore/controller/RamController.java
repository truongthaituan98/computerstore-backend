package com.spring.computerstore.controller;

import com.spring.computerstore.model.Ram;
import com.spring.computerstore.service.RamService;
import com.spring.computerstore.validation.MapValidationError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/api/ram")
@RestController
public class RamController {
    @Autowired
    RamService ramService;
    @Autowired
    MapValidationError mapValidationError;
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<Ram>> getAllRam() {
        List<Ram> ramList = ramService.getAllRam();
        return new ResponseEntity<>(ramList, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/save", method = RequestMethod.POST)
    public ResponseEntity<?> saveRam(@Valid @RequestBody Ram ram, BindingResult bindingResult) {
        ResponseEntity<?> errorMap = mapValidationError.mapValidationError(bindingResult);
        if(errorMap!=null) return errorMap;
        Ram ram1 = ramService.saveOrUpdate(ram);
        return new ResponseEntity<>(ram1, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Ram> getRamById(@PathVariable("id") Long ramId) {
        Ram ram = ramService.findRamById(ramId);
        return new ResponseEntity<>(ram, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Ram> updateRam(@PathVariable("id") Long ramId, @RequestBody Ram ram) {
        Ram ramCurrent = ramService.findRamById(ramId);
        ramCurrent.setBusSpeed(ram.getBusSpeed());
        ramCurrent.setMemory(ram.getMemory());
        ramCurrent.setType(ram.getType());
        Ram ramUpdate = ramService.saveOrUpdate(ramCurrent);
        return new ResponseEntity<>(ramUpdate, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteRamById(@PathVariable("id") Long ramId) {
        ramService.deleteRamById(ramId);
        return new ResponseEntity<>("Ram with ID "+ramId+" was deleted", HttpStatus.OK);
    }
}
