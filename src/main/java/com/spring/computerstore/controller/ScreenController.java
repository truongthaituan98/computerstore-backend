package com.spring.computerstore.controller;

import com.spring.computerstore.model.Screen;
import com.spring.computerstore.service.ScreenService;
import com.spring.computerstore.validation.MapValidationError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/api/screen")
@RestController
public class ScreenController {
    @Autowired
    ScreenService screenService;
    @Autowired
    MapValidationError mapValidationError;
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<Screen>> getAllScreen() {
        List<Screen> screenList = screenService.getAllScreen();
        return new ResponseEntity<>(screenList, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/save", method = RequestMethod.POST)
    public ResponseEntity<?> saveScreen(@Valid @RequestBody Screen screen, BindingResult bindingResult) {
        ResponseEntity<?> errorMap = mapValidationError.mapValidationError(bindingResult);
        if(errorMap!=null) return errorMap;
        Screen screen1 = screenService.saveOrUpdate(screen);
        return new ResponseEntity<>(screen1, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Screen> getScreenById(@PathVariable("id") Long screenId) {
        Screen screen = screenService.findScreenById(screenId);
        return new ResponseEntity<>(screen, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Screen> updateScreen(@PathVariable("id") Long screenId, @RequestBody Screen screen) {
        Screen screenCurrent = screenService.findScreenById(screenId);
        screenCurrent.setResolutionsOfScreen(screen.getResolutionsOfScreen());
        screenCurrent.setSize(screen.getSize());
        screenCurrent.setTechnology(screen.getTechnology());
        screenCurrent.setTouchPanel(screen.getTouchPanel());
        Screen screenUpdate = screenService.saveOrUpdate(screenCurrent);
        return new ResponseEntity<>(screenUpdate, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteScreenById(@PathVariable("id") Long screenId) {
        screenService.deleteScreenById(screenId);
        return new ResponseEntity<>("Screen with ID "+screenId+" was deleted", HttpStatus.OK);
    }
}
