package com.spring.computerstore.controller;

import com.spring.computerstore.model.HardDisk;
import com.spring.computerstore.service.HardDiskService;
import com.spring.computerstore.validation.MapValidationError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/api/hard_disk")
@RestController
public class HardDiskController {
    @Autowired
    HardDiskService hardDiskService;
    @Autowired
    MapValidationError mapValidationError;
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<HardDisk>> getAllHardDisk() {
        List<HardDisk> hrdDiskList = hardDiskService.getAllHardDisk();
        return new ResponseEntity<>(hrdDiskList, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/save", method = RequestMethod.POST)
    public ResponseEntity<?> saveHardDisk(@Valid @RequestBody HardDisk hardDisk, BindingResult bindingResult) {
        ResponseEntity<?> errorMap = mapValidationError.mapValidationError(bindingResult);
        if(errorMap!=null) return errorMap;
        HardDisk hardDisk1 = hardDiskService.saveOrUpdate(hardDisk);
        return new ResponseEntity<>(hardDisk1, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<HardDisk> getHardDiskById(@PathVariable("id") Long hardDiskId) {
        HardDisk hardDisk = hardDiskService.findHardDiskById(hardDiskId);
        return new ResponseEntity<>(hardDisk, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.PUT)
    public ResponseEntity<HardDisk> updateHardDisk(@PathVariable("id") Long hardDiskId, @RequestBody HardDisk hardDisk) {
        HardDisk hardDiskCurrent = hardDiskService.findHardDiskById(hardDiskId);
        hardDiskCurrent.setName(hardDisk.getName());
        hardDiskCurrent.setCapacity(hardDisk.getCapacity());
        HardDisk hardDiskUpdate = hardDiskService.saveOrUpdate(hardDiskCurrent);
        return new ResponseEntity<>(hardDiskUpdate, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteHardDiskById(@PathVariable("id") Long hardDiskId) {
        hardDiskService.deleteHardDiskById(hardDiskId);
        return new ResponseEntity<>("Hard Disk with ID "+hardDiskId+" was deleted", HttpStatus.OK);
    }
}
