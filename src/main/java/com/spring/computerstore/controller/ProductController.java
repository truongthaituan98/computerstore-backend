package com.spring.computerstore.controller;

import com.spring.computerstore.model.*;
import com.spring.computerstore.service.*;
import com.spring.computerstore.validation.MapValidationError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/api/product")
public class ProductController {
    @Autowired
    ProductService productService;
    @Autowired
    BatteryService batteryService;
    @Autowired
    BrandService brandService;
    @Autowired
    CPUService cpuService;
    @Autowired
    GraphicsCardService graphicsCardService;
    @Autowired
    HardDiskService hardDiskService;
    @Autowired
    OperatingSystemService operatingSystemService;
    @Autowired
    RamService ramService;
    @Autowired
    ScreenService screenService;
    @Autowired
    MapValidationError mapValidationError;
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<Product>> getAllProduct() {
        List<Product> productList = productService.getAllProduct();
        return new ResponseEntity<>(productList, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/save", method = RequestMethod.POST)
    public ResponseEntity<?> saveProduct(@Valid @RequestBody Product product, BindingResult bindingResult) {
        ResponseEntity<?> errorMap = mapValidationError.mapValidationError(bindingResult);
        if(errorMap!=null) return errorMap;
        if(product.getBattery().getBatteryId() != null){
            Long batteryId = product.getBattery().getBatteryId();
            Battery battery = batteryService.findBatteryById(batteryId);
            product.setBattery(battery);
        }
        if(product.getBrand().getBrandId() != null) {
            Long brandId = product.getBrand().getBrandId();
            Brand brand = brandService.findBrandById(brandId);
            product.setBrand(brand);
        }
        if(product.getCpu().getCpuId() != null) {
            Long CPUId = product.getCpu().getCpuId();
            CPU cpu = cpuService.findCPUById(CPUId);
            product.setCpu(cpu);
        }
        if(product.getGraphicsCard().getGraphicsCardId() != null) {
            Long graphicsCardId = product.getGraphicsCard().getGraphicsCardId();
            GraphicsCard graphicsCard = graphicsCardService.findGraphicsCardById(graphicsCardId);
            product.setGraphicsCard(graphicsCard);
        }
        if(product.getHardDisk().getHardDiskId() != null) {
            Long hardDiskId = product.getHardDisk().getHardDiskId();
            HardDisk hardDisk = hardDiskService.findHardDiskById(hardDiskId);
            product.setHardDisk(hardDisk);
        }
        if(product.getOperatingSystem().getOperatingSystemId() != null) {
            Long operatingSystemId = product.getOperatingSystem().getOperatingSystemId();
            OperatingSystem operatingSystem = operatingSystemService.findOperatingSystemById(operatingSystemId);
            product.setOperatingSystem(operatingSystem);
        }
        if(product.getRam().getRamId() != null) {
            Long ramId = product.getRam().getRamId();
            Ram ram = ramService.findRamById(ramId);
            product.setRam(ram);
        }
        if(product.getScreen().getScreenId() != null) {
            Long screenId = product.getScreen().getScreenId();
            Screen screen = screenService.findScreenById(screenId);
            product.setScreen(screen);
        }
        Product product1 = productService.saveOrUpdate(product);
        return new ResponseEntity<>(product1, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Product> getProductById(@PathVariable("id") Long productId) {
        Product product = productService.findProductById(productId);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateProduct(@PathVariable("id") Long productId, @Valid @RequestBody Product product, BindingResult bindingResult) {
        ResponseEntity<?> errorMap = mapValidationError.mapValidationError(bindingResult);
        if(errorMap!=null) return errorMap;
        Product productCurrent = productService.findProductById(productId);
        productCurrent.setName(product.getName());
        productCurrent.setPrice(product.getPrice());
        productCurrent.setColor(product.getColor());
        productCurrent.setWeight(product.getWeight());
        productCurrent.setSize(product.getSize());
        productCurrent.setSound(product.getSound());
        productCurrent.setQuantity(product.getQuantity());
        productCurrent.setImage(product.getImage());
        productCurrent.setSummary(product.getSummary());
        Long batteryId = product.getBattery().getBatteryId();
        Long brandId = product.getBrand().getBrandId();
        Long CPUId = product.getCpu().getCpuId();
        Long graphicsCardId = product.getGraphicsCard().getGraphicsCardId();
        Long hardDiskId = product.getHardDisk().getHardDiskId();
        Long operatingSystemId = product.getOperatingSystem().getOperatingSystemId();
        Long ramId = product.getRam().getRamId();
        Long screenId = product.getScreen().getScreenId();
        if(batteryId != null){
            Battery battery = batteryService.findBatteryById(batteryId);
            productCurrent.setBattery(battery);
        }
        if(brandId != null) {
            Brand brand = brandService.findBrandById(brandId);
            productCurrent.setBrand(brand);
        }
        if(CPUId != null) {
            CPU cpu = cpuService.findCPUById(CPUId);
            productCurrent.setCpu(cpu);
        }
        if(graphicsCardId != null) {
            GraphicsCard graphicsCard = graphicsCardService.findGraphicsCardById(graphicsCardId);
            productCurrent.setGraphicsCard(graphicsCard);
        }
        if(hardDiskId != null) {
            HardDisk hardDisk = hardDiskService.findHardDiskById(hardDiskId);
            productCurrent.setHardDisk(hardDisk);
        }
        if(operatingSystemId != null) {
            OperatingSystem operatingSystem = operatingSystemService.findOperatingSystemById(operatingSystemId);
            productCurrent.setOperatingSystem(operatingSystem);
        }
        if(ramId != null) {
            Ram ram = ramService.findRamById(ramId);
            productCurrent.setRam(ram);
        }
        if(screenId != null) {
            Screen screen = screenService.findScreenById(screenId);
            productCurrent.setScreen(screen);
        }
        Product productUpdate = productService.saveOrUpdate(productCurrent);
        return new ResponseEntity<>(productUpdate, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProductById(@PathVariable("id") Long productId) {
        productService.deleteProductById(productId);
        return new ResponseEntity<>("Product with ID "+productId+" was deleted", HttpStatus.OK);
    }
}
