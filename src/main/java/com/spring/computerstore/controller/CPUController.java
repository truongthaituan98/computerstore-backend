package com.spring.computerstore.controller;

import com.spring.computerstore.model.CPU;
import com.spring.computerstore.service.CPUService;
import com.spring.computerstore.validation.MapValidationError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/api/cpu")
@RestController
public class CPUController {
    @Autowired
    CPUService cpuService;
    @Autowired
    MapValidationError mapValidationError;
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<CPU>> getAllCPU() {
        List<CPU> CPUList = cpuService.getCPUList();
        return new ResponseEntity<>(CPUList, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/save", method = RequestMethod.POST)
    public ResponseEntity<?> saveCPU(@Valid @RequestBody CPU cpu, BindingResult bindingResult) {
        ResponseEntity<?> errorMap = mapValidationError.mapValidationError(bindingResult);
        if(errorMap!=null) return errorMap;
        CPU cpu1 = cpuService.saveOrUpdate(cpu);
        return new ResponseEntity<>(cpu1, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<CPU> getCPUById(@PathVariable("id") Long CPUId) {
        CPU cpu = cpuService.findCPUById(CPUId);
        return new ResponseEntity<>(cpu, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.PUT)
    public ResponseEntity<CPU> updateCPU(@PathVariable("id") Long CPUId, @RequestBody CPU cpu) {
        CPU cpuCurrent = cpuService.findCPUById(CPUId);
        cpuCurrent.setName(cpu.getName());
        cpuCurrent.setBufferMemory(cpu.getBufferMemory());
        cpuCurrent.setSpeed(cpu.getSpeed());
        cpuCurrent.setTechnology(cpu.getTechnology());
        cpuCurrent.setTurboBoostSpeed(cpu.getTurboBoostSpeed());
        CPU cpuUpdate = cpuService.saveOrUpdate(cpuCurrent);
        return new ResponseEntity<>(cpuUpdate, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteCPUById(@PathVariable("id") Long CPUId) {
        cpuService.deleteCPUById(CPUId);
        return new ResponseEntity<>("CPU with ID "+CPUId+" was deleted", HttpStatus.OK);
    }
}
