package com.spring.computerstore.controller;

import com.spring.computerstore.dto.AuthRequest;
import com.spring.computerstore.dto.ResponseAuth;
import com.spring.computerstore.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class AuthenticateController {
    @Autowired
    private AccountService accountService;

    @PostMapping(value = "/api/authenticate", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseAuth> generateToken(@RequestBody AuthRequest authRequest) {
        // Check user/admin
        ResponseAuth response = accountService.login(authRequest.getUserName(), authRequest.getPassword());
        return new ResponseEntity<>(response,HttpStatus.OK);
    }
}
