package com.spring.computerstore.controller;

import com.spring.computerstore.model.OperatingSystem;
import com.spring.computerstore.service.OperatingSystemService;
import com.spring.computerstore.validation.MapValidationError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/api/operating_system")
@RestController
public class OperatingSystemController {
    @Autowired
    OperatingSystemService operatingSystemService;
    @Autowired
    MapValidationError mapValidationError;
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<OperatingSystem>> getAllOperatingSystem() {
        List<OperatingSystem> operatingSystemList = operatingSystemService.getAllOperatingSystem();
        return new ResponseEntity<>(operatingSystemList, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/save", method = RequestMethod.POST)
    public ResponseEntity<?> saveOperatingSystem(@Valid @RequestBody OperatingSystem operatingSystem, BindingResult bindingResult) {
        ResponseEntity<?> errorMap = mapValidationError.mapValidationError(bindingResult);
        if(errorMap!=null) return errorMap;
        OperatingSystem operatingSystem1 = operatingSystemService.saveOrUpdate(operatingSystem);
        return new ResponseEntity<>(operatingSystem1, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<OperatingSystem> getOperatingSystemById(@PathVariable("id") Long operatingSystemId) {
        OperatingSystem operatingSystem = operatingSystemService.findOperatingSystemById(operatingSystemId);
        return new ResponseEntity<>(operatingSystem, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.PUT)
    public ResponseEntity<OperatingSystem> updateOperatingSystem(@PathVariable("id") Long operatingSystemId, @RequestBody OperatingSystem operatingSystem) {
        OperatingSystem operatingSystemCurrent = operatingSystemService.findOperatingSystemById(operatingSystemId);
        operatingSystemCurrent.setName(operatingSystem.getName());
        OperatingSystem operatingSystemUpdate = operatingSystemService.saveOrUpdate(operatingSystemCurrent);
        return new ResponseEntity<>(operatingSystemUpdate, HttpStatus.OK);
    }

    @RequestMapping(value = "/admin/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteOperatingSystemById(@PathVariable("id") Long operatingSystemId) {
        operatingSystemService.deleteOperatingSystemById(operatingSystemId);
        return new ResponseEntity<>("Operating System with ID "+operatingSystemId+" was deleted", HttpStatus.OK);
    }
}
