package com.spring.computerstore.repository;

import com.spring.computerstore.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    Account findByuserName(String username);
    @Modifying
    @Transactional
    @Query(value = "update Account acc set acc.status = :status where acc.id = :accountId", nativeQuery = true)
    void deleteAccountsByAccountId(@Param("accountId") Long accountId, @Param("status") boolean status);
    @Query(value = "SELECT count(ac.email)>0 FROM Account ac WHERE ac.email = :email", nativeQuery = true)
    int isEmailExist(@Param("email") String email);
    @Query(value = "SELECT count(ac.userName)>0 FROM Account ac WHERE ac.userName = :userName", nativeQuery = true)
    int isUserNameExist(@Param("userName") String userName);
}
