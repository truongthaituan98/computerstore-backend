package com.spring.computerstore.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "ram")
public class Ram {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ramId;
    @Column(name = "memory")
    private Float memory;
    @Column(name = "type")
    private String type;
    @Column(name = "bus_speed")
    private Float busSpeed;
    @OneToMany(mappedBy = "ram", cascade = CascadeType.ALL)
    private Set<Product> products;

    public Ram(Float memory, String type, Float busSpeed) {
        this.memory = memory;
        this.type = type;
        this.busSpeed = busSpeed;
    }

    public Ram() { }

    public Long getRamId() {
        return ramId;
    }

    public void setRamId(Long ramId) {
        this.ramId = ramId;
    }

    public Float getMemory() {
        return memory;
    }

    public void setMemory(Float memory) {
        this.memory = memory;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Float getBusSpeed() {
        return busSpeed;
    }

    public void setBusSpeed(Float busSpeed) {
        this.busSpeed = busSpeed;
    }
}
