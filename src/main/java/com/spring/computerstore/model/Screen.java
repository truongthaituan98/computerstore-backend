package com.spring.computerstore.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "screen")
public class Screen {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long screenId;
    @Column(name = "size")
    private Float size;
    @Column(name = "resolutions_of_screen")
    private String resolutionsOfScreen;
    @Column(name = "technology")
    private String technology;
    @Column(name = "touch_panel")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean touchPanel;
    @OneToMany(mappedBy = "screen", cascade = CascadeType.ALL)
    private Set<Product> products;

    public Screen(Float size, String resolutionsOfScreen, String technology, Boolean touchPanel) {
        this.size = size;
        this.resolutionsOfScreen = resolutionsOfScreen;
        this.technology = technology;
        this.touchPanel = touchPanel;
    }

    public Screen() {
    }

    public Long getScreenId() {
        return screenId;
    }

    public void setScreenId(Long screenId) {
        this.screenId = screenId;
    }

    public Float getSize() {
        return size;
    }

    public void setSize(Float size) {
        this.size = size;
    }

    public String getResolutionsOfScreen() {
        return resolutionsOfScreen;
    }

    public void setResolutionsOfScreen(String resolutionsOfScreen) {
        this.resolutionsOfScreen = resolutionsOfScreen;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public Boolean getTouchPanel() {
        return touchPanel;
    }

    public void setTouchPanel(Boolean touchPanel) {
        this.touchPanel = touchPanel;
    }
}
