package com.spring.computerstore.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Entity
@Table(name = "cpu")
public class CPU {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cpuId;
    @Column(name = "name")
    @NotBlank(message = "CPU name is required")
    private String name;
    @Column(name = "speed")
    @NotBlank(message = "CPU speed is required")
    private Float speed;
    @Column(name = "technology")
    @NotBlank(message = "CPU technology is required")
    private String technology;
    @Column(name = "buffer_memory")
    private String bufferMemory;
    @Column(name = "turbo_boost_speed")
    private Float turboBoostSpeed;
    @OneToMany(mappedBy = "cpu", cascade = CascadeType.ALL)
    private Set<Product> products;

    public CPU(String name, Float speed, String technology, String bufferMemory, Float turboBoostSpeed) {
        this.name = name;
        this.speed = speed;
        this.technology = technology;
        this.bufferMemory = bufferMemory;
        this.turboBoostSpeed = turboBoostSpeed;
    }

    public CPU() { }

    public Long getCpuId() {
        return cpuId;
    }

    public void setCpuId(Long cpuId) {
        this.cpuId = cpuId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getSpeed() {
        return speed;
    }

    public void setSpeed(Float speed) {
        this.speed = speed;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public String getBufferMemory() {
        return bufferMemory;
    }

    public void setBufferMemory(String bufferMemory) {
        this.bufferMemory = bufferMemory;
    }

    public Float getTurboBoostSpeed() {
        return turboBoostSpeed;
    }

    public void setTurboBoostSpeed(Float turboBoostSpeed) {
        this.turboBoostSpeed = turboBoostSpeed;
    }
}
