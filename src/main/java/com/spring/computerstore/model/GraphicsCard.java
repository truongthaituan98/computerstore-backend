package com.spring.computerstore.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Entity
@Table(name = "graphics_card")
public class GraphicsCard {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long graphicsCardId;
    @Column(name = "name")
    @NotBlank(message = "Graphics Card name is required")
    private String name;
    @Column(name = "memory")
    private Float memory;
    @Column(name = "type")
    private String type;
    @OneToMany(mappedBy = "graphicsCard", cascade = CascadeType.ALL)
    private Set<Product> products;

    public GraphicsCard(String name, Float memory, String type) {
        this.name = name;
        this.memory = memory;
        this.type = type;
    }

    public GraphicsCard() { }

    public Long getGraphicsCardId() {
        return graphicsCardId;
    }

    public void setGraphicsCardId(Long graphicsCardId) {
        this.graphicsCardId = graphicsCardId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getMemory() {
        return memory;
    }

    public void setMemory(Float memory) {
        this.memory = memory;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
