package com.spring.computerstore.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Entity
@Table(name = "battery")
public class Battery {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long batteryId;
    @Column(name = "battery_info")
    @NotBlank(message = "The battery information is required")
    private String batteryInfo;
    @Column(name = "battery_expiry_date")
    private Integer batteryExpiryDate;
    @Column(name = "charger")
    private String charger;
    @OneToMany(mappedBy = "battery", cascade = CascadeType.ALL)
    private Set<Product> products;

    public Battery(String batteryInfo, Integer batteryExpiryDate, String charger) {
        this.batteryInfo = batteryInfo;
        this.batteryExpiryDate = batteryExpiryDate;
        this.charger = charger;
    }

    public Battery() { }

    public Long getBatteryId() {
        return batteryId;
    }

    public void setBatteryId(Long batteryId) {
        this.batteryId = batteryId;
    }

    public String getBatteryInfo() {
        return batteryInfo;
    }

    public void setBatteryInfo(String batteryInfo) {
        this.batteryInfo = batteryInfo;
    }

    public Integer getBatteryExpiryDate() {
        return batteryExpiryDate;
    }

    public void setBatteryExpiryDate(Integer batteryExpiryDate) {
        this.batteryExpiryDate = batteryExpiryDate;
    }

    public String getCharger() {
        return charger;
    }

    public void setCharger(String charger) {
        this.charger = charger;
    }

}

