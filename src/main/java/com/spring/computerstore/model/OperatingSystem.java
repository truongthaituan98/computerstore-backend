package com.spring.computerstore.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Entity
@Table(name = "operating_system")
public class OperatingSystem {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long operatingSystemId;
    @Column(name = "name")
    @NotBlank(message = "Operating System name is required")
    private String name;
    @OneToMany(mappedBy = "operatingSystem", cascade = CascadeType.ALL)
    private Set<Product> products;

    public OperatingSystem(String name) {
        this.name = name;
    }

    public OperatingSystem() {
    }

    public Long getOperatingSystemId() {
        return operatingSystemId;
    }

    public void setOperatingSystemId(Long operatingSystemId) {
        this.operatingSystemId = operatingSystemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
