package com.spring.computerstore.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Entity
@Table(name = "hard_disk")
public class HardDisk {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long hardDiskId;
    @Column(name = "name")
    @NotBlank(message = "Hard Disk name is required")
    private String name;
    @Column(name = "capacity")
    private Float capacity;
    @OneToMany(mappedBy = "hardDisk", cascade = CascadeType.ALL)
    private Set<Product> products;

    public HardDisk(String name, Float capacity) {
        this.name = name;
        this.capacity = capacity;
    }

    public HardDisk() {
    }

    public Long getHardDiskId() {
        return hardDiskId;
    }

    public void setHardDiskId(Long hardDiskId) {
        this.hardDiskId = hardDiskId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getCapacity() {
        return capacity;
    }

    public void setCapacity(Float capacity) {
        this.capacity = capacity;
    }

}
