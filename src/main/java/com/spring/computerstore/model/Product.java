package com.spring.computerstore.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Entity
@Table(name = "product")
public class Product {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    @NotBlank(message = "Product name is required")
    private String name;
    @Column(name = "price")
    @Positive(message = "The quantity should be positive number")
    private Float price;
    @Column(name = "color")
    private String color;
    @Column(name = "weight")
    @Positive(message = "The quantity should be positive number")
    private Float weight;
    @Column(name = "size")
    private String size;
    @Column(name = "sound")
    private String sound;
    @Column(name = "resolutions_of_wc")
    private String resolutionsOfWC;
    @ManyToOne
    @JoinColumn(name = "cpu", referencedColumnName = "id")
    private CPU cpu;
    @ManyToOne
    @JoinColumn(name = "hard_disk", referencedColumnName = "id")
    private HardDisk hardDisk;
    @ManyToOne
    @JoinColumn(name = "ram", referencedColumnName = "id")
    private Ram ram;
    @ManyToOne
    @JoinColumn(name = "graphics_card", referencedColumnName = "id")
    private GraphicsCard graphicsCard;
    @ManyToOne
    @JoinColumn(name = "screen", referencedColumnName = "id")
    private Screen screen;
    @ManyToOne
    @JoinColumn(name = "battery", referencedColumnName = "id")
    private Battery battery;
    @ManyToOne
    @JoinColumn(name = "operating_system", referencedColumnName = "id")
    private OperatingSystem operatingSystem;
    @ManyToOne
    @JoinColumn(name = "brand", referencedColumnName = "id")
    private Brand brand;
    @Column(name = "quantity")
    @PositiveOrZero(message = "The quantity should be positive number or 0")
    private Integer quantity;
    @Column(name = "summary")
    private String summary;
    @Column(name = "image")
    private String image;

    public Product(String name, Float price, String color, Float weight, String size, String sound, String resolutionsOfWC,
                   CPU cpu, HardDisk hardDisk, Ram ram, GraphicsCard graphicsCard, Screen screen, Battery battery, OperatingSystem operatingSystem,
                   Brand brand, Integer quantity, String summary, String image) {
        this.name = name;
        this.price = price;
        this.color = color;
        this.weight = weight;
        this.size = size;
        this.sound = sound;
        this.resolutionsOfWC = resolutionsOfWC;
        this.cpu = cpu;
        this.hardDisk = hardDisk;
        this.ram = ram;
        this.graphicsCard = graphicsCard;
        this.screen = screen;
        this.battery = battery;
        this.operatingSystem = operatingSystem;
        this.brand = brand;
        this.quantity = quantity;
        this.summary = summary;
        this.image = image;
    }

    public Product() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public String getResolutionsOfWC() {
        return resolutionsOfWC;
    }

    public void setResolutionsOfWC(String resolutionsOfWC) {
        this.resolutionsOfWC = resolutionsOfWC;
    }

    public CPU getCpu() {
        return cpu;
    }

    public void setCpu(CPU cpu) {
        this.cpu = cpu;
    }

    public HardDisk getHardDisk() {
        return hardDisk;
    }

    public void setHardDisk(HardDisk hardDisk) {
        this.hardDisk = hardDisk;
    }

    public Ram getRam() {
        return ram;
    }

    public void setRam(Ram ram) {
        this.ram = ram;
    }

    public GraphicsCard getGraphicsCard() {
        return graphicsCard;
    }

    public void setGraphicsCard(GraphicsCard graphicsCard) {
        this.graphicsCard = graphicsCard;
    }

    public Screen getScreen() {
        return screen;
    }

    public void setScreen(Screen screen) {
        this.screen = screen;
    }

    public Battery getBattery() {
        return battery;
    }

    public void setBattery(Battery battery) {
        this.battery = battery;
    }

    public OperatingSystem getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(OperatingSystem operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
